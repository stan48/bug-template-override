
### Summary

<!-- Brief summmary of the incident. Here user report may be included. -->

### Impact

<!-- Who was impacted? What is potential cost of the incident? -->

### Root causes

<!-- What is the root cause? What caused this issue to happen? -->

### Trigger

<!-- What triggered the issue? e.g. deployment, misconfiguration -->

### Resolution

<!-- How it should or it was resolved? -->

### Links / references

/due in 1 day
/cc @gitlab
/subscribe
